package com.example.demo;

// import java.sql.Date;

public record Data(
    int id,
    String name,
    String groupName,
    String modifiedDate
){}
