// package com.example.demo;

// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.kafka.annotation.KafkaListener;
// import org.springframework.stereotype.Service;

// import java.io.FileWriter;
// import java.io.IOException;

// @Service
// public class SubscriberService {

//     @Value("${output.csv.path}")
//     private String outputPath;

//     @KafkaListener(topics = "${kafka.topic.name}", groupId = "${spring.kafka.consumer.group-id}")
//     public void consume(String message) {
//         try (FileWriter writer = new FileWriter(outputPath, true)) {
//             writer.append(message).append("\n");
//         } catch (IOException e) {
//             e.printStackTrace();
//         }
//     }
// }
