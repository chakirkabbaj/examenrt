package com.example.demo;

// import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
// import org.springframework.batch.item.database.JdbcBatchItemWriter;
// import org.springframework.batch.item.database.JdbcCursorItemReader;
// import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
// import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
// import org.springframework.jdbc.core.DataClassRowMapper;
import org.springframework.jdbc.support.JdbcTransactionManager;

@Configuration
public class JobConfiguration {
   
    @Bean
    public Job job(JobRepository jobRepository, Step step) {
        return new JobBuilder("Job",jobRepository)
            .start(step)
            .build();
    }

    @Bean
    public Step step(
        JobRepository jobRepository, JdbcTransactionManager jdbcTransactionManager,
        ItemReader<Data> DataFileReader, ItemWriter<Data> DataTableWriter,
        ItemProcessor<Data, Data> DataProcessor
    ){
        return new StepBuilder("fileIngestion", jobRepository)
            .<Data, Data>chunk(1, jdbcTransactionManager)
            .reader(DataFileReader)
            .processor(DataProcessor)
            .writer(DataTableWriter)
            .build();
    }


    @Bean
    public FlatFileItemReader<Data> DataFileReader(){
        return new FlatFileItemReaderBuilder<Data>()
            .name("DataFileReader")
            .resource(new FileSystemResource("staging/output.csv"))
            .delimited()
            .names("id","name","groupName","modifiedDate")
            .targetType(Data.class)
            .build();
    }


    @Bean
    public FlatFileItemWriter<Data> DataFileWriter(){
        return new FlatFileItemWriterBuilder<Data>()
            .resource(new FileSystemResource("staging/processed-output.csv"))
            .name("DataFileWriter")
            .delimited()
            .names("id","name","groupName","modifiedDate")
            .build();
    }

    @Bean
    public DataProcessor DataProcessor(){
        return new DataProcessor();
    }
   
}
